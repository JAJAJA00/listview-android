package com.example.my;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Main2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
    }
    public void next (View v){
        Intent intent = new Intent(Main2.this,Main3.class);
        startActivity(intent);
    }
    public void back (View v){
        Intent intent = new Intent(Main2.this,MainActivity.class);
        startActivity(intent);
    }
    public void home (View v){
        Intent intent = new Intent(Main2.this,MainActivity.class);
        startActivity(intent);
    }
}